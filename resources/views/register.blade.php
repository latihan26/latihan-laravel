@extends('layout.master')

@section('judul')
    <h1>Sign Up</h1>
@endsection

@section('content')
    <h1>Buat Akun Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="welcome1" method="post">
        @csrf
        <label>First Name :</label><br><br>
        <input type="text" name="Firstname"><br><br>
        <label>Last Name :</label><br><br>
        <input type="text" name="Lastname"><br><br>
        <label>Gender</label> <br><br>
        <input type="radio" id="Male" name="Gender" value="male"> Male <br><br>
        <input type="radio" id="Female" name="Gender" value="female"> Female <br><br>
        <label>Nationality</label> <br><br>
        <select name="Nationality" id=""> <br><br>
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option>
        </select> <br><br>
        <label>Language Spoken</label> <br><br>
        <input type="checkbox" name="Language1"> Bahasa Indonesia <br><br>
        <input type="checkbox" name="Language2"> English <br><br>
        <input type="checkbox" name="Language3"> Others <br><br>
        <label>Bio</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br><br>
        <input type="submit" value="Sign Up">
@endsection