@extends('layout.master')

@section('judul')
    <h1>Halaman Cast</h1>
@endsection

@section('content')
@auth
<a href="/cast/create" class="btn btn-secondary mb-3">Tambah Cast</a>
@endauth
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>
                    @auth
                    <form action="/cast/{{$item->id}}" method="POST">
                        <a href="/cast/{{$item->id}}" class="btn btn-info btm-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btm-sm">Edit</a>
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sml" value="Delete">
                    </form>
                    @endauth
                    @guest
                    <a href="/cast/{{$item->id}}" class="btn btn-info btm-sm">Detail</a>
                    @endguest
                </td>
            </tr>
        @empty
            <h1>Data Tidak Ada</h1>
        @endforelse
    </tbody>
  </table>
@endsection