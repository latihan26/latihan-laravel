@extends('layout.master')

@section('judul')
    <h1>Welcome!</h1>
@endsection

@section('content')
<body>
    <h1>SELAMAT DATANG! {{$nama1}} {{$nama2}} </h1>
    <h2>Terima kasih telah bergabung di Website kami. Media belajar kita bersama!</h2>
@endsection