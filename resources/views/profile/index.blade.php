@extends('layout.master')

@section('judul')
    <h1>Halaman Update Profile</h1>
@endsection

@section('content')
<form action="/profil/{{$profile->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama User</label>
      <input type="text" value="{{$profile->user->name}}" class="form-control" disabled>
    </div>
    <div class="form-group">
      <label>Email User</label>
      <input type="text" value="{{$profile->user->email}}" class="form-control" disabled>
    </div>

    <div class="form-group">
      <label>Umur</label>
      <input type="number" name="umur" value="{{$profile->umur}}" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" class="form-control">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Alamat</label>
      <input type="text" name="alamat" value="{{$profile->alamat}}" class="form-control">
    </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
 </form>
@endsection
