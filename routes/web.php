<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@register');

Route::post('/welcome1', 'AuthController@signup');

Route::get('/data-table', 'IndexController@table');


// Create
Route::get('/cast/create', 'CastController@create'); // form menuju from Create
Route::post('/cast', 'CastController@store'); // Route untuk menyimpan data ke database

// Read
Route::get('/cast', 'CastController@index'); // Route List Cast
Route::get('/cast/{cast_id}', 'CastController@show'); // Route Detail Kategori

// Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); // Route Menuju Form Edit
Route::put('/cast/{cast_id}', 'CastController@update'); // Route Untuk Update Data Berdasarkan ID di Database

// Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); // Route Untuk Menghapus Data di Database

// Update Profile
Route::resource('profil', 'ProfileController')->only(['index', 'update']);

Auth::routes();
